#!/bin/bash

sudo pacman -S --noconfirm --needed xdg-user-dirs xdg-utils linux-zen-headers noto-fonts noto-fonts-cjk noto-fonts-emoji noto-fonts-extra base-devel git reflector xorg xorg-xinit xf86-video-amdgpu mesa lib32-mesa libva-mesa-driver lib32-libva-mesa-driver vulkan-radeon lib32-vulkan-radeon vulkan-icd-loader lib32-vulkan-icd-loader vulkan-headers opencl-mesa lib32-opencl-mesa opencl-icd-loader lib32-opencl-icd-loader vkd3d lib32-vkd3d libva-vdpau-driver lib32-libva-vdpau-driver bluez bluez-utils alsa-utils lib32-alsa-plugins pipewire pipewire-alsa pipewire-pulse pipewire-jack lib32-pipewire lib32-pipewire-jack wireplumber pavucontrol ntfs-3g tree bash-completion firefox ffmpeg ffmpeg4.4 hunspell hunspell-en_us libnotify bspwm sxhkd alacritty rofi lxappearance pcmanfm xarchiver arj cpio lha lrzip lzip lzop p7zip unarj unrar unzip zip mousepad imwheel


#mousepad dependency gspell
#make imwheel config and autostart

install -Dm755 /usr/share/doc/bspwm/examples/bspwmrc ~/.config/bspwm/bspwmrc
install -Dm644 /usr/share/doc/bspwm/examples/sxhkdrc ~/.config/sxhkd/sxhkdrc

echo "exec bspwm" >> ~/.xinitrc

sudo echo "Section "InputClass"
	Identifier "Kingsis Corporation ZOWIE Gaming mouse"
	Driver "libinput"
	MatchIsPointer "yes"
	Option "AccelProfile" "flat"
	Option "AccelSpeed" "0"
EndSection" >> /etc/X11/xorg.conf.d/50-mouse-acceleration.conf
