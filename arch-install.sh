#!/bin/bash

ln -sf /usr/share/zoneinfo/Europe/Zagreb /etc/localtime
hwclock --systohc
sed -i '178s/.//' /etc/locale.gen
sed -i '179s/.//' /etc/locale.gen
locale-gen
echo "LANG=en_US.UTF-8" >> /etc/locale.conf
echo "BigTiddyGothArch" >> /etc/hostname
echo "127.0.0.1 localhost" >> /etc/hosts
echo "::1       localhost" >> /etc/hosts
echo "127.0.1.1 BigTiddyGothArch.localdomain    BigTiddyGothArch" >> /etc/hosts
echo root:l33t | chpasswd
useradd -m jack
echo jack:l33t | chpasswd
usermod -aG wheel,audio,disk,floppy,input,kvm,optical,scanner,storage,video jack

sed -i '37s/.//' /etc/pacman.conf
sed -i '93s/.//' /etc/pacman.conf
sed -i '94s/.//' /etc/pacman.conf

pacman -Sy --noconfirm --needed grub efibootmgr os-prober efitools networkmanager mtools dosfstools sudo

grub-install --target=x86_64-efi --efi-directory=/boot/efi --bootloader-id=BigTiddyGothArch
sed -i '63s/.//' /etc/default/grub
grub-mkconfig -o /boot/grub/grub.cfg

systemctl enable NetworkManager
#systemctl enable fstrim.timer

echo "jack ALL=(ALL) ALL" >> /etc/sudoers.d/jack

printf "\e[1;32mDone! Type exit, umount -R and reboot.\e[0m"
